use std::error::Error;

use clap::Clap;
use simple_logger::SimpleLogger;

use commands::account::{Account, AccountSubCommand};
use commands::passwords::{Pass, PassSubCommand};

mod commands;

#[derive(Clap)]
#[clap(
    version = "0.1",
    author = "Sylvain Willy <zorvalt@pm.me>",
    about = "An online password manager"
)]
struct Opts {
    #[clap(short, long, parse(from_occurrences))]
    verbose: u8,

    #[clap(subcommand)]
    sub_cmd: SubCommand,
}

#[derive(Clap)]
enum SubCommand {
    Account(Account),
    List(Search),
    Pass(Pass),
    ShareOffers(ShareOffers),
}

#[derive(Clap)]
struct Search {
    #[clap(short)]
    pub user: String,

    #[clap(short)]
    pattern: Option<String>,
}

#[derive(Clap)]
struct ShareOffers {
    #[clap(short)]
    pub user: String,
}

fn init_logger(verbosity: u8) {
    let log_level = match verbosity {
        1 => log::LevelFilter::Info,
        2 => log::LevelFilter::Debug,
        3 => log::LevelFilter::Trace,
        _ => log::LevelFilter::Warn,
    };
    SimpleLogger::from_env()
        .with_level(log_level)
        .init()
        .unwrap();

    if verbosity > 3 {
        log::warn!(
            "Invalid verbosity of {}: set by default to warning level",
            verbosity
        );
    }
}

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn Error>> {
    let opts: Opts = Opts::parse();
    init_logger(opts.verbose);

    match opts.sub_cmd {
        SubCommand::Account(sub_cmd) => match sub_cmd.sub_cmd {
            AccountSubCommand::Shared => {
                commands::account::review_shared_offers_op(sub_cmd.user).await?
            }
            AccountSubCommand::Register => commands::account::register_op(sub_cmd.user).await?,
            AccountSubCommand::UpdatePassword => {
                commands::account::update_password_op(sub_cmd.user).await?
            }
            AccountSubCommand::Delete => commands::account::delete_op(sub_cmd.user).await?,
        },
        SubCommand::List(sub_cmd) => {
            commands::passwords::list_op(sub_cmd.user, sub_cmd.pattern).await?
        }
        SubCommand::Pass(sub_cmd) => match sub_cmd.sub_cmd {
            PassSubCommand::Get(get_info) => {
                commands::passwords::get_op(sub_cmd.user, get_info).await?
            }
            PassSubCommand::Add(add_info) => {
                commands::passwords::add_op(sub_cmd.user, add_info).await?
            }
            PassSubCommand::Update(update_info) => {
                commands::passwords::update_op(sub_cmd.user, update_info).await?
            }
            PassSubCommand::Remove(remove_info) => {
                commands::passwords::remove_op(sub_cmd.user, remove_info).await?
            }
            PassSubCommand::Share(share_info) => {
                commands::passwords::share_op(sub_cmd.user, share_info).await?
            }
        },
        SubCommand::ShareOffers(info) => {
            commands::account::review_shared_offers_op(info.user).await?
        }
    }

    Ok(())
}
