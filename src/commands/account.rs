use anyhow::Result;
use clap::Clap;
use qrcode::render::unicode;
use qrcode::QrCode;

use super::CliOTPHandler;
use zorpasslib::registry_manager::ShareOffer;
use zorpasslib::{
    get_registry_manager, otp::OTPHandler, AccountManager, LoginCryptoManager, MasterKeyManager,
};

#[derive(Clap)]
pub struct Account {
    #[clap(short)]
    pub user: String,

    #[clap(subcommand)]
    pub sub_cmd: AccountSubCommand,
}

#[derive(Clap)]
pub enum AccountSubCommand {
    Register,
    UpdatePassword,
    Delete,
    Shared,
}

pub async fn register_op(username: String) -> Result<()> {
    let password = rpassword::read_password_from_tty(Some("Password: "))?;
    let password2 = rpassword::read_password_from_tty(Some("Repeat password: "))?;

    if password != password2 {
        return Err(anyhow::Error::msg("Passwords do not match"));
    }

    let mut otp_handler = CliOTPHandler::new();
    let mut first_call = true;
    let mut otp_callback = |secret: &str| {
        if first_call {
            display_otp_secret(&username, secret);
            first_call = false;
        }

        otp_handler.get_otp()
    };

    zorpasslib::AccountManager::register(username.clone(), password, &mut otp_callback).await?;
    println!("User {} registered", username);
    Ok(())
}

fn display_otp_secret(username: &str, secret: &str) {
    let otp_uri = format!(
        "otpauth://totp/ZorPass:{}?secret={}&issuer=ZorPass",
        username, secret
    );
    log::debug!("OTP URI = {}", otp_uri);

    let code = QrCode::new(otp_uri).unwrap();
    let image = code
        .render::<unicode::Dense1x2>()
        .dark_color(unicode::Dense1x2::Light)
        .light_color(unicode::Dense1x2::Dark)
        .build();
    println!("{}", image);
}

pub async fn update_password_op(username: String) -> Result<()> {
    let password =
        rpassword::read_password_from_tty(Some("Unlock your account to proceed. Password: "))?;
    let salt = AccountManager::get_salt(username.clone()).await?;
    let mut login_crypto_manager = LoginCryptoManager::new(password.as_ref(), salt.as_ref())?;
    let passkey = login_crypto_manager.get_passkey().to_string();
    let login_secrets_key = login_crypto_manager.get_login_secrets_key();
    let otp_handler = Box::new(CliOTPHandler::new());
    let mut api = AccountManager::new(
        username.clone(),
        passkey.clone(),
        login_secrets_key,
        otp_handler,
    )
    .await?;

    let new_password = rpassword::read_password_from_tty(Some("New password: "))?;
    let new_password2 = rpassword::read_password_from_tty(Some("Repeat new password: "))?;
    if new_password != new_password2 {
        return Err(anyhow::Error::msg("New passwords do not match"));
    }

    // Get current master key
    let mut master_key_manager = MasterKeyManager::new(&mut api).await?;
    let encrypted_master_key = master_key_manager.get_master_key().await?;

    // Re-derive passkey from new password and re-encrypt master key
    let new_master_key =
        login_crypto_manager.update_master_key_password(&encrypted_master_key, &new_password)?;
    let new_passkey = login_crypto_manager.get_passkey().to_string();

    // Update both passkey and master key
    master_key_manager
        .update_master_key_and_passkey(&new_master_key, new_passkey)
        .await?;

    Ok(())
}

pub async fn delete_op(username: String) -> Result<()> {
    let password = rpassword::read_password_from_tty(Some("Password: "))?;

    let salt = AccountManager::get_salt(username.clone()).await?;
    let login_crypto_manager = LoginCryptoManager::new(password.as_ref(), salt.as_ref())?;
    let passkey = login_crypto_manager.get_passkey().to_string();
    let login_secrets_key = login_crypto_manager.get_login_secrets_key();
    let otp_handler = Box::new(CliOTPHandler::new());
    let mut api = AccountManager::new(
        username.clone(),
        passkey.clone(),
        login_secrets_key,
        otp_handler,
    )
    .await?;
    match api.delete_account().await {
        Ok(_) => println!("Account deleted"),
        Err(e) => println!("Error: {:?}", e),
    }
    Ok(())
}

pub async fn review_shared_offers_op(username: String) -> Result<()> {
    let password =
        rpassword::read_password_from_tty(Some("Unlock your account to proceed. Password: "))?;

    let otp_handler = Box::new(CliOTPHandler::new());
    let mut registry_manager =
        get_registry_manager(username.clone(), password, otp_handler).await?;
    let share_offers = registry_manager.list_share_offers(username).await?;

    if share_offers.is_empty() {
        println!("There is no pending share offer");
    } else {
        for offer in share_offers {
            match handle_share_offer(&offer) {
                ShareAction::Accept => {
                    println!("Accepted");
                    registry_manager
                        .add_password(
                            offer.offer.website,
                            offer.offer.username,
                            offer.offer.password.as_bytes(),
                        )
                        .await?;
                    println!("Password successfully imported");
                    registry_manager.remove_share_offer(offer.offer_id).await?;
                }
                ShareAction::Refuse => {
                    println!("Refused");
                    registry_manager.remove_share_offer(offer.offer_id).await?;
                }
                ShareAction::Skip => println!("Skipped"),
            }
        }
    }

    Ok(())
}

fn handle_share_offer(share_offer: &ShareOffer) -> ShareAction {
    println!("Would you like to import this share offer?");
    println!(
        "\tUser \"{}\" offers to share the account: {}@{}",
        share_offer.sender, share_offer.offer.username, share_offer.offer.website
    );
    {
        use std::io::Write;
        print!("Choose an action (one letter) between Accept/Refuse/Skip: [arS]  ");
        std::io::stdout().flush().ok();
    }

    let mut input = String::new();
    if std::io::stdin().read_line(&mut input).is_ok() {
        match &input[..] {
            "a\n" => ShareAction::Accept,
            "r\n" => ShareAction::Refuse,
            _ => ShareAction::Skip,
        }
    } else {
        ShareAction::Skip
    }
}

enum ShareAction {
    Accept,
    Refuse,
    Skip,
}
