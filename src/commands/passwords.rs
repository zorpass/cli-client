use anyhow::Result;
use clap::Clap;

use super::CliOTPHandler;

#[derive(Clap)]
pub struct Pass {
    #[clap(short)]
    pub user: String,

    #[clap(subcommand)]
    pub sub_cmd: PassSubCommand,
}

#[derive(Clap)]
pub enum PassSubCommand {
    Get(Get),
    Add(Add),
    Update(Update),
    Remove(Remove),
    Share(Share),
}

#[derive(Clap)]
pub struct Get {
    #[clap(short)]
    pub website: String,

    #[clap(short)]
    pub username: String,
}

#[derive(Clap)]
pub struct Add {
    #[clap(short)]
    pub website: String,

    #[clap(short)]
    pub username: String,
}

#[derive(Clap)]
pub struct Update {
    #[clap(short)]
    pub website: String,

    #[clap(short)]
    pub username: String,
}

#[derive(Clap)]
pub struct Remove {
    #[clap(short)]
    pub website: String,

    #[clap(short)]
    pub username: String,
}

#[derive(Clap)]
pub struct Share {
    #[clap(short)]
    pub website: String,

    #[clap(short)]
    pub username: String,

    #[clap(short)]
    pub recepient: String,
}

pub async fn list_op(username: String, pattern: Option<String>) -> Result<()> {
    println!("Listing registered accounts");
    let account_password =
        rpassword::read_password_from_tty(Some("Unlock your account. Password: "))?;

    let otp_handler = Box::new(CliOTPHandler::new());
    let mut registry_manager =
        zorpasslib::get_registry_manager(username.clone(), account_password, otp_handler).await?;
    let file_list = registry_manager.account_list(pattern).await?;

    if file_list.is_empty() {
        println!("No account saved yet.");
        println!(
            "You can add one with the command:\n\tpass -u {} add -w <website> -u <username>",
            username
        );
    } else {
        println!("Registered accounts:");
        for file in file_list {
            println!("\t- {}@{}", file.username, file.website);
        }
    }

    Ok(())
}

pub async fn add_op(username: String, add_info: Add) -> Result<()> {
    println!("Adding file {}@{}", add_info.username, add_info.website);
    let account_password =
        rpassword::read_password_from_tty(Some("Unlock your account to proceed. Password: "))?;

    let otp_handler = Box::new(CliOTPHandler::new());
    let mut registry_manager =
        zorpasslib::get_registry_manager(username, account_password, otp_handler).await?;

    let pass_prompt = format!("Password for {}@{}: ", add_info.username, add_info.website);
    let pass_password = rpassword::read_password_from_tty(Some(&pass_prompt))?;
    registry_manager
        .add_password(
            add_info.website,
            add_info.username,
            pass_password.as_bytes(),
        )
        .await?;
    Ok(())
}

pub async fn get_op(username: String, get_info: Get) -> Result<()> {
    println!("Getting file {}@{}", get_info.username, get_info.website);
    let account_password =
        rpassword::read_password_from_tty(Some("Unlock your account to proceed. Password: "))?;

    let otp_handler = Box::new(CliOTPHandler::new());
    let mut registry_manager =
        zorpasslib::get_registry_manager(username, account_password, otp_handler).await?;
    let file = registry_manager
        .get_account(&get_info.website, &get_info.username)
        .await?;

    match file {
        Some(content) => println!(
            "Account {}@{}: {}",
            &get_info.username, &get_info.website, content
        ),
        None => println!("Account not found"),
    }

    Ok(())
}

pub async fn update_op(username: String, update_info: Update) -> Result<()> {
    println!(
        "Updating file {}@{}",
        update_info.username, update_info.website
    );
    let account_password =
        rpassword::read_password_from_tty(Some("Unlock your account to proceed. Password: "))?;

    let otp_handler = Box::new(CliOTPHandler::new());
    let mut registry_manager =
        zorpasslib::get_registry_manager(username, account_password, otp_handler).await?;

    let pass_prompt = format!(
        "New password for {}@{}: ",
        update_info.username, update_info.website
    );
    let pass_password = rpassword::read_password_from_tty(Some(&pass_prompt))?;
    registry_manager
        .update_file(
            update_info.website,
            update_info.username,
            pass_password.as_bytes(),
        )
        .await?;
    Ok(())
}

pub async fn remove_op(username: String, remove_info: Remove) -> Result<()> {
    println!(
        "!!! Removing file {}@{} !!!",
        remove_info.username, remove_info.website
    );
    let account_password =
        rpassword::read_password_from_tty(Some("Unlock your account to proceed. Password: "))?;

    let otp_handler = Box::new(CliOTPHandler::new());
    let mut registry_manager =
        zorpasslib::get_registry_manager(username, account_password, otp_handler).await?;

    registry_manager
        .remove_file(remove_info.website, remove_info.username)
        .await?;

    println!("Successfully removed file");

    Ok(())
}

pub async fn share_op(username: String, share_info: Share) -> Result<()> {
    println!(
        "You are about to share the file {}@{} with the user {}",
        share_info.username, share_info.website, share_info.recepient
    );
    let account_password =
        rpassword::read_password_from_tty(Some("Unlock your account to proceed. Password: "))?;

    let otp_handler = Box::new(CliOTPHandler::new());
    let mut registry_manager =
        zorpasslib::get_registry_manager(username, account_password, otp_handler).await?;
    registry_manager
        .add_share_offer(
            share_info.website,
            share_info.username,
            share_info.recepient,
        )
        .await?;

    println!("Successfully shared file");
    Ok(())
}
