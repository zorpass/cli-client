use std::io::{self, BufRead};

pub mod account;
pub mod passwords;

use zorpasslib::otp::{OTPError, OTPHandler};

struct CliOTPHandler {
    counter: u8,
}
impl CliOTPHandler {
    const MAX_ATTEMPTS: u8 = 3;
    pub fn new() -> CliOTPHandler {
        CliOTPHandler { counter: 0 }
    }
}
impl OTPHandler for CliOTPHandler {
    fn get_otp(&mut self) -> Result<u32, OTPError> {
        if self.counter > 0 {
            if self.counter >= Self::MAX_ATTEMPTS {
                println!("Too many invalid tries. Aborting.");
                return Err(OTPError::TooManyAttempts);
            } else {
                println!(
                    "Invalid OTP: try {}/{}. Please try again.",
                    self.counter,
                    Self::MAX_ATTEMPTS
                );
            }
        }
        self.counter += 1;

        Ok(read_otp())
    }
}

fn read_otp() -> u32 {
    use std::io::Write;

    loop {
        print!("Please enter a valid (6 digits) OTP: ");
        std::io::stdout().flush().ok();
        let mut line = String::new();
        let stdin = io::stdin();
        stdin.lock().read_line(&mut line).unwrap();

        match line.trim().parse() {
            Ok(otp) => break otp,
            Err(err) => println!("Error: {}", err),
        }
    }
}
