# ZorPass CLI client
Official ZorPass CLI client based on [ZorPassLib](https://gitlab.com/zorpass/zorpasslib).

## Description
ZorPass is an online password manager developed as an academic exercise and written in Rust. It was designed with the idea that an attacker able to read the database should not be able to exploit the data. Although everything seems to work and I have designed the software with care, it has not been reviewed and is not guaranteed to be secure in practice.

## Features

- Authentication
  - Challenge-response authentication protocol
  - 2FA with TOTP
- Users can share passwords between them
- Server privacy
  - Neither files nor filenames are readable by the server
  - Files are each encrypted with a different key
    - One file key leak should not compromise the others
  - Server unable to associate users to their files except when uploading/downloading them
  - Server unable to associate two users sharing passwords together except when creating the "share offer"

## Setup

```bash
git clone git@gitlab.com:zorpass/cli-client.git && cd cli-client
git submodule update --init --recursive
cargo build --release
```
The executable will be at `target/release/zorpass-cli-client`.

## Usage

- Basic(for ZorPass account `$USERNAME`)
  1. Create an account with `zorpass-cli-client account -u $USERNAME register`
  2. Add a new password with `zorpass-cli-client pass -u $USERNAME add -w $WEBISTE -u WEBSITE_ACCOUNT`
  3. Fetch a password with `zorpass-cli-client pass -u $USERNAME get -w $WEBISTE -u WEBSITE_ACCOUNT`
  4. List passwords with `zorpass-cli-client pass -u $USERNAME list`
- Password sharing(with other account `$OTHER`)
  1. Offer to share a password with `zorpass-cli-client pass -u $USERNAME share -w $WEBISTE -u WEBSITE_ACCOUNT -r $OTHER`
  2. List(accept/reject) share offers to you with `zorpass-cli-client account -u $USERNAME shared`

